**SceneBuilder Examples**


* PatternTextField

This examples shows you how to reuse the SceneBuilder AutoSuggestEditor to provide your own entries.

First you have to create your property editor and a factory which you then use to annotate
the setter method in your custom component.

```Java
public class PatternTextField extends TextField {
    private String pattern;

    @PropertyEditorFactorySetup(SuggestPatternPropertyEditorFactorySetup.class)
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
```

Because we reused the SceneBuilder's ``InlineListEditor`` we were able to provide our own list of values.

![PatternTextField in Scenebuilder](doc/PatternTextField_SB.png)


Which then end up in the FXML

![PatternTextField resulting FXML](doc/PatternTextField_FXML.png)


To keep things "simple" I've reused as much as possible from the SceneBuilder editor itself.

As long as you extend your property editor from ``com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.PropertyEditor``
SceneBuilder will be happy to server you.

A list of editors as source for your own can be found in the SceneBuilder source repository at 
https://bitbucket.org/gluon-oss/scenebuilder/src/5a37a872e23261fbacbce4a5ad7485693309e9c2/kit/src/main/java/com/oracle/javafx/scenebuilder/kit/editor/panel/inspector/editors
