package at.datenwort.scenebuider.componentExample;

import com.oracle.javafx.scenebuilder.kit.editor.EditorController;
import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.EditorItem;
import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.InlineListEditor;
import com.oracle.javafx.scenebuilder.kit.metadata.property.ValuePropertyMetadata;

import java.util.Arrays;
import java.util.Set;

public class SuggestPatternPropertyEditor extends InlineListEditor {

    private final SuggestPatternEditorItem editorItem;
    private EditorController editorController;

    public SuggestPatternPropertyEditor(ValuePropertyMetadata propMeta, Set<Class<?>> selectedClasses, EditorController editorController) {
        super(propMeta, selectedClasses);
        this.editorController = editorController;

        SuggestPatternEditorItem editorItem = new SuggestPatternEditorItem(
                this,
                propMeta.getName().getName(),
                toString(propMeta.getDefaultValueObject()),
                Arrays.asList(
                        "#,##0",
                        "#,##0.00",
                        "dd/MM/yyyy",
                        "HH:mm"
                ));

        this.editorItem = editorItem;
        addItem(editorItem);
    }

    public void reset(ValuePropertyMetadata valuePropertyMetadata, Set<Class<?>> selectedClasses, boolean removeAll, EditorController editorController) {
        super.reset(valuePropertyMetadata, selectedClasses, removeAll);
        this.editorController = editorController;
    }

    @Override
    public void commit(EditorItem source) {
        try {
            Object value = getValue();
            userUpdateValueProperty(value);
        } catch (Exception ex) {
            editorController.getMessageLog().logWarningMessage(
                    "inspector.style.valuetypeerror", ex.getMessage());
        }
    }

    private String toString(Object object) {
        if (object == null) {
            return "";
        }

        return object.toString();
    }

    @Override
    public Object getValue() {
        Object ret = editorItem.getValue();
        if (ret == null) {
            return "";
        }
        return ret;
    }

    @Override
    public void setValue(Object value) {
        setValueGeneric(value);
        if (isSetValueDone()) {
            return;
        }
        if (value == null) {
            reset();
            return;
        }

        editorItem.setValue(value);
    }

    @Override
    public void requestFocus() {
        editorItem.requestFocus();
    }
}
