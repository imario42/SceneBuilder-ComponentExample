package at.datenwort.scenebuider.componentExample;

import com.gluon.sceneBuilder.propertyEditorFactory.PropertyEditorFactory;
import com.oracle.javafx.scenebuilder.kit.editor.EditorController;
import com.oracle.javafx.scenebuilder.kit.metadata.property.ValuePropertyMetadata;

import java.util.Set;

public class SuggestPatternPropertyEditorFactorySetup implements PropertyEditorFactory<SuggestPatternPropertyEditor> {
    @Override
    public SuggestPatternPropertyEditor createEditor(ValuePropertyMetadata valuePropertyMetadata, Set<Class<?>> selectedClasses, EditorController editorController) {
        return new SuggestPatternPropertyEditor(valuePropertyMetadata, selectedClasses, editorController);
    }

    @Override
    public void resetEditor(SuggestPatternPropertyEditor fancyTextFieldPropertyEditor, ValuePropertyMetadata valuePropertyMetadata, Set<Class<?>> selectedClasses, EditorController editorController) {
        fancyTextFieldPropertyEditor.reset(valuePropertyMetadata, selectedClasses, false, editorController);
    }

    @Override
    public Class<SuggestPatternPropertyEditor> getEditorClass() {
        return SuggestPatternPropertyEditor.class;
    }
}
