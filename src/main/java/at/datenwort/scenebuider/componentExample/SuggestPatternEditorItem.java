package at.datenwort.scenebuider.componentExample;

import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.AutoSuggestEditor;
import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.EditorItem;
import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.editors.EditorUtils;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

import java.util.List;

public class SuggestPatternEditorItem extends AutoSuggestEditor implements EditorItem {

    private final SuggestPatternPropertyEditor editor;
    private String currentValue;

    public SuggestPatternEditorItem(SuggestPatternPropertyEditor editor, String name, String defaultValue, List<String> suggestedList) {
        super(name, defaultValue, suggestedList, false);
        this.editor = editor;

        setTextEditorBehavior(getTextField(), event -> {
            if (getValue().equals(currentValue)) {
                // no change
                return;
            }
            editor.commit(SuggestPatternEditorItem.this);
            if (event != null && event.getSource() instanceof TextField) {
                ((TextField) event.getSource()).selectAll();
            }

            currentValue = EditorUtils.toString(getValue());
        });
    }

    public void setValue(Object text) {
        super.setValue(text);
        currentValue = EditorUtils.toString(text);
    }

    @Override
    public Node getNode() {
        return getTextField();
    }

    @Override
    public void reset() {
        getTextField().setText("");
    }

    @Override
    public void setValueAsIndeterminate() {
        handleIndeterminate(getTextField());
    }

    @Override
    public MenuItem getMoveUpMenuItem() {
        return null;
    }

    @Override
    public MenuItem getMoveDownMenuItem() {
        return null;
    }

    @Override
    public MenuItem getRemoveMenuItem() {
        return null;
    }

    @Override
    public Button getPlusButton() {
        return null;
    }

    @Override
    public Button getMinusButton() {
        return null;
    }
}
