package at.datenwort.scenebuider.componentExample;

import com.gluon.sceneBuilder.propertyEditorFactory.PropertyEditorFactorySetup;
import javafx.scene.control.TextField;

public class PatternTextField extends TextField {
    private String pattern;

    public String getPattern() {
        return pattern;
    }

    @PropertyEditorFactorySetup(SuggestPatternPropertyEditorFactorySetup.class)
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
